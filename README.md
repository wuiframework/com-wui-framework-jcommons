# com-wui-framework-jcommons v1.1.2

> WUI Framework library focused on Java based back-end projects.

## Requirements

This library does not have any special requirements, but it depends on the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). See the WUI Builder requirements before you build this project.

## Project build

The project build is fully automated. For more information about the project build, see the 
[WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

An interface with batch or bash scripts is prepared for all common project tasks:

* `install`
* `build`
* `clean`
* `test`
* `docs`

> NOTE: All scripts are stored in the **./bin/batch** or **./bin/bash** sub-folder in the project root folder.

## Documentation

This project provides automatically generated documentation in [Javadoc](http://docs.oracle.com/javase/8/docs/technotes/tools/windows/javadoc.html) 
from the Java source by running the `docs` command.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v1.1.2
Update of SCR and history ordering.
### v1.1.1
Added support for configs in JSONP format.
### v1.1.0
Added implementation of LiveContentWrapper and bi-directional communication with browser instance. Lint and structure clean up.
### v1.0.3
Unification of unit tests by UnitestRunner class. Documentation update.
### v1.0.2
Native part of WUI Builder. Added ability to launch WUI Connector on process start.
### v1.0.1
Basic builder integration.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar - B48779, 
Copyright (c) 2017 [NXP](http://nxp.com/)
