/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
package Com.Wui.Framework.Jcommons.Exceptions;

public class WuiIllegalArgumentException extends IllegalArgumentException {

    public WuiIllegalArgumentException(final String $message, final Throwable $cause) {
        super("\n\n          " + $message + "\n          Cause: " + $cause.getMessage());
    }

    public WuiIllegalArgumentException(final String $message, final String $reason) {
        super("\n\n          " + $message + "\n          Cause: " + $reason);
    }
}
