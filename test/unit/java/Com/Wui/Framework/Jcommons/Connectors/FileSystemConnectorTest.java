package Com.Wui.Framework.Jcommons.Connectors;

import Com.Wui.Framework.Assert;
import Com.Wui.Framework.Jcommons.Connectors.Args.ArchiveOptions;
import Com.Wui.Framework.UnitTest;

import static Com.Wui.Framework.Jcommons.Connectors.FileSystemConnector.*;

public class FileSystemConnectorTest extends UnitTest {

    private static final String testDataPath = "build/test_data";

    public void __IgnoretestUnpack() throws Exception {
        Unpack("resource/target.zip", new ArchiveOptions().Output(testDataPath));
        Assert.equal(Exists(testDataPath), true);
        Delete(testDataPath);
        Assert.equal(Exists(testDataPath), false);
    }
}
