package Com.Wui.Framework.Jcommons;

import Com.Wui.Framework.Assert;
import Com.Wui.Framework.UnitTest;

public class TestExample extends UnitTest {

    private static final String testDataPath = "build/test_data";

    public void before() {
        System.out.println("before");
    }

    public void after() {
        System.out.println("after");
    }

    public void setUp() {
        System.out.println("setUp");
    }

    public void tearDown() {
        System.out.println("tearDown");
    }

    public void __ignoreTestCase() throws Exception {
        System.out.println("THIS IS SOME TEST");
    }

    public void testCase1() throws Exception {
        System.out.println("testCase1");
        Assert.equal(1, 1);
    }

    public void testCase2() throws Exception {
        System.out.println("testCase2");
        Assert.throwsException(() -> System.out.println(10 / 0));
    }
}
